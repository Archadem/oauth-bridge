# OAuth Bridge

[![pipeline status](https://gitlab.com/Archadem/oauth-bridge/badges/master/pipeline.svg)](https://gitlab.com/Archadem/oauth-bridge/commits/master) [![coverage report](https://gitlab.com/Archadem/oauth-bridge/badges/master/coverage.svg)](https://gitlab.com/Archadem/oauth-bridge/-/commits/master)

Serveur express permettant de gérer la récupération des tokens d'authentification pour l'API de Spotify. Gestion du déploiement automatique sur Heroku avec la CI mise en place.

Lien pour récupérer les **CLIENT_ID** et **CLIENT_SECRET** de votre application : [Dashboard Spotify](https://developer.spotify.com/dashboard/****)

## Fonctionnalités

Chemin d'accès : **/login**, **/exchange**, **/refresh**.

## Configuration

Pour faire fonctionner le serveur, il faut définir certaines variables d'environnement à la racine du projet. 

```bash
export SPOTIFY_CLIENT_ID="<Votre Client ID Spotify>"
export SPOTIFY_CLIENT_SECRET="<Votre Client Secret Spotify>"
export REDIRECT_URI="<L'url de redirection de votre application>"
export ENCRYPTION_SECRET="<Clé secrète utilisée pour chiffrer certaines données>"
```

## Installation et mise en route

```bash
npm i
npm start
```

## Tests

```bash
npm test
```
