const cryptojs = require('crypto-js')
const express = require('express');
const request = require('request');
const cors = require('cors');

const app = express();
app.use(cors({ credentials: true, origin: true }));

const redirectURI = process.env.REDIRECT_URI || 'http://localhost:4200';

const spotifyRequest = params => {
    return new Promise((resolve, reject) => {
        request.post('https://accounts.spotify.com/api/token', {
            headers: {
                'Authorization': 'Basic ' +  (new Buffer.from(
                    process.env.SPOTIFY_CLIENT_ID + ':' + process.env.SPOTIFY_CLIENT_SECRET
                ).toString('base64'))
            },
            form: params,
            json: true
        },
        (error, response) => {
            error ? reject(error): resolve(response);
        });
    })
    .then(response => {
        return Promise.resolve(response.body);
    })
    .catch(error => {
        return Promise.reject({
            body: { error: error.toString() },
            statusCode: 500
        }); 
    })
}

app.get('/login', (req, res) => {
    const url = new URL('https://accounts.spotify.com/authorize?');

    url.searchParams.append('scope', 'user-read-private user-read-email');
    url.searchParams.append('client_id', process.env.SPOTIFY_CLIENT_ID);
    url.searchParams.append('redirect_uri', redirectURI);
    url.searchParams.append('response_type', 'code');

    res.redirect(url.toString());
});

app.get('/exchange', (req, res) => {
    const params = req.query;
    if (!params.code) return res.status(400).json({
        'error': 'Parameter code missing in request'
    });  

    spotifyRequest({
        grant_type: "authorization_code",
        redirect_uri: redirectURI,
        code: params.code
    }).then(response => {
        const authData = {
            "refresh_token": encrypt(response.refresh_token),
            "access_token": response.access_token,
            "expires_in": response.expires_in,
        };
        return res.json(authData);
    }).catch(error => {
        return res.status(error.statusCode).json(error.body);
    });
});

app.get('/refresh', (req, res) => {
    const params = req.query;
    if (!params.refresh_token) return res.status(400).json({
        'error': 'Parameter refresh_token missing in request'
    });

    spotifyRequest({
        refresh_token: decrypt(params.refresh_token),
        grant_type: "refresh_token"
    }).then(response => {
        const authData = {
            "access_token": response.access_token,
            "expires_in": response.expires_in,
        };
        return res.json(authData);
    }).catch(error => {
        return res.status(error.statusCode).json(error.body);
    });
});

function encrypt(text) {
    return cryptojs.AES.encrypt(text, process.env.ENCRYPTION_SECRET).toString();
};
   
function decrypt(text) {
    const bytes = cryptojs.AES.decrypt(text, process.env.ENCRYPTION_SECRET);
    return bytes.toString(cryptojs.enc.Utf8);
};

const port = process.env.PORT || 8888;
app.listen(port);

exports.app = app;
exports.decrypt = decrypt;