const supertest = require('supertest');
const server = require('./server');
const nock = require('nock');
const chai = require('chai');
const url = require("url");

const api = supertest.agent(server.app);
const should = chai.should();

describe('Spotify auth process', () => {
    beforeEach(() => {
        process.env.SPOTIFY_CLIENT_SECRET = 'my_client_secret';
        process.env.SPOTIFY_CLIENT_ID = 'my_client_id';
        process.env.ENCRYPTION_SECRET = 'encrypt_key';
    })

    afterEach(() => {
        delete process.env.SPOTIFY_CLIENT_SECRET;
        delete process.env.SPOTIFY_CLIENT_ID;
        delete process.env.ENCRYPTION_SECRET;
    })

    it('should redirect to spotify authorize url', (done) => {
        api.get('/login').end((err, res) => {
            should.not.exist(err);
            
            should.exist(res);
            should.exist(res.header['location']);
            // parse the redirect url passed to the response
            const redirectURL = url.parse(res.header['location'], true);

            redirectURL.query.scope.should.equal('user-read-private user-read-email');
            redirectURL.query.client_id.should.equal(process.env.SPOTIFY_CLIENT_ID);
            redirectURL.query.redirect_uri.should.equal('http://localhost:4200');

            redirectURL.host.should.equal('accounts.spotify.com');
            redirectURL.pathname.should.equal('/authorize');
            done();
        });
    });

    it('should test exchange on success', (done) => {
        nock('https://accounts.spotify.com/api').post('/token').reply(200, { access_token: 'mon_super_token', refresh_token: 'mon_refresh_token', expires_in: '3600' });
        api.get('/exchange?code=test_code').end((err, res) => {
            res.status.should.equal(200);
            should.exist(res.body);
            should.not.exist(err);

            res.body.expires_in.should.equal('3600');
            res.body.access_token.should.equal('mon_super_token');
            server.decrypt(res.body.refresh_token).should.equal('mon_refresh_token');
            done();
        });
    });

    it('should test refresh on success', (done) => {
        nock('https://accounts.spotify.com/api').post('/token').reply(200, { access_token: 'mon_super_token', expires_in: '3600' });
        api.get('/refresh?refresh_token=test_token').end((err, res) => {
            res.status.should.equal(200);
            should.exist(res.body);
            should.not.exist(err);

            res.body.expires_in.should.equal('3600');
            res.body.access_token.should.equal('mon_super_token');
            done();
        });
    });

    it('should test exchange on error', (done) => {
        nock('https://accounts.spotify.com/api').post('/token').replyWithError('Invalid parameter code provided');
        api.get('/exchange?code=test_code').end((err, res) => {
            res.status.should.equal(500);
            should.exist(res.body);
            should.not.exist(err);
            
            res.body.error.should.contain('Invalid parameter code provided');
            done();
        });
    });

    it('should test refresh on error', (done) => {
        nock('https://accounts.spotify.com/api').post('/token').replyWithError('Invalid refresh_token provided');
        api.get('/refresh?refresh_token=test_token').end((err, res) => {
            res.status.should.equal(500);
            should.exist(res.body);
            
            res.body.error.should.contain('Invalid refresh_token provided');
            done();
        });
    });

    it('should test missing param on exchange', (done) => {
        api.get('/exchange').end((err, res) => {
            res.status.should.equal(400);
            should.exist(res.body.error);
            should.not.exist(err);

            res.body.error.should.equal(
                'Parameter code missing in request'
            );
            done();
        });
    });

    it('should test missing param on refresh', (done) => {
        api.get('/refresh').end((err, res) => {
            res.status.should.equal(400);
            should.exist(res.body.error);
            should.not.exist(err);
            
            res.body.error.should.equal(
                'Parameter refresh_token missing in request'
            );
            done();
        });
    });
});